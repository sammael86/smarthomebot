﻿namespace SmartHomeBot.Model
{
    public enum DeviceType
    {
        None,
        Lamp,
        Kettle,
        Socket,
    }
}
