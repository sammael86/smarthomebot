﻿using Microsoft.EntityFrameworkCore;

namespace SmartHomeBot.Model
{
    class SmartHomeContext : DbContext
    {
        public DbSet<TelegramUser> TelegramUsers { get; set; }
        public DbSet<Device> Devices { get; set; }

        public SmartHomeContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(Program.settings.DBConnectionString);
        }
    }
}
