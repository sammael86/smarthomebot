﻿using Telegram.Bot.Types.ReplyMarkups;

namespace SmartHomeBot.Model
{
    struct Answer
    {
        public string Text;
        public IReplyMarkup Markup;
    }
}
