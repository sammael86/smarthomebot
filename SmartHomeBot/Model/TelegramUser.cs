﻿using System.Collections.Generic;

namespace SmartHomeBot.Model
{
    class TelegramUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public List<Device> Devices { get; set; }
    }
}
