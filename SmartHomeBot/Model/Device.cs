﻿namespace SmartHomeBot.Model
{
    public class Device
    {
        public string Id { get; set; }
        public DeviceType Type { get; set; }
    }
}