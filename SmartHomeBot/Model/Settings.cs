﻿namespace SmartHomeBot.Model
{
    public class Settings
    {
        public string TelegramToken { get; set; }
        public string DBConnectionString { get; set; }
        public RabbitMQSettings RabbitMQ { get; set; }
        public class RabbitMQSettings
        {
            public string Server { get; set; }
            public string User { get; set; }
            public string Password { get; set; }
        }
    }
}