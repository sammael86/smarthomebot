﻿using SmartHomeBot.Model;
using SmartHomeBot.States;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace SmartHomeBot
{
    internal class Context : IState
    {
        private IState state;
        public int UserId { get; }
        public TelegramUser User { get; internal set; }

        public Context(int userId)
        {
            UserId = userId;
            state = new StateDefault(this);
        }

        public void ChangeState(IState state) => this.state = state;

        public async Task<Answer> Command(Message command) => await state.Command(command);
    }
}