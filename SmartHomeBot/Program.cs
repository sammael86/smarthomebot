using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SmartHomeBot.Model;

namespace SmartHomeBot
{
    public class Program
    {
        public static Settings settings;

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    settings = configuration.GetSection(nameof(Settings)).Get<Settings>();
                    services.AddSingleton(settings);
                    services.AddHostedService<Worker>();
                }).UseWindowsService();
    }
}
