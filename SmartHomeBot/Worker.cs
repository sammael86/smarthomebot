using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Telegram.Bot;

namespace SmartHomeBot
{
    public class Worker : BackgroundService
    {
        private readonly static ConcurrentBag<Context> _contexts = new ConcurrentBag<Context>();
        private readonly TelegramBotClient _bot;

        public Worker()
        {
            _bot = new TelegramBotClient(Program.settings.TelegramToken);
            _bot.OnMessage += async (sender, eventArgs) => { await Bot_OnMessage(sender, eventArgs); };
        }

        private async Task Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs eventArgs)
        {
            var userId = eventArgs.Message.From.Id;
            Context context = _contexts.FirstOrDefault(c => c.UserId == userId);
            if (context is null)
            {
                context = new Context(userId);
                _contexts.Add(context);
            }

            var answer = await context.Command(eventArgs.Message);

            await _bot.SendTextMessageAsync(userId, answer.Text, disableWebPagePreview: true, replyMarkup: answer.Markup);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(() => _bot.StartReceiving(cancellationToken: stoppingToken));
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _bot.StopReceiving();
            return base.StopAsync(cancellationToken);
        }
    }
}
