﻿using SmartHomeBot.Model;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace SmartHomeBot.States
{
    interface IState
    {
        Task<Answer> Command(Message command);
    }
}
